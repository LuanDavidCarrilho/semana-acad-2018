import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { ConferenceApp } from './app.component';
import { MapPage } from '../pages/map/map';
import { ProgramacaoPage } from '../pages/programacao/programacao';
import { ProgramacaoFilterPage } from '../pages/programacao-filter/programacao-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { SemanaAcadPage } from "../pages/semana-acad/semana-acad";
import { CredenciamentoPage } from '../pages/credenciamento/credenciamento';
import { RoomPage } from '../pages/room/room';
import { RoomDetailPage } from '../pages/room-detail/room-detail';
import { ApOraisPage } from '../pages/ap-orais/ap-orais';
import { ListMinicursosPage } from '../pages/list-minicursos/list-minicursos';
import { MinicursoDetailPage } from '../pages/minicurso-detail/minicurso-detail';
import { ListOficinaPage } from '../pages/list-oficina/list-oficina';
import { OficinaDetailPage } from '../pages/oficina-detail/oficina-detail';
import { MesaRedondaPage } from '../pages/mesa-redonda/mesa-redonda';
import { LivroResumoPage } from '../pages/livro-resumo/livro-resumo';
import { CadernoResumoPage } from '../pages/caderno-resumo/caderno-resumo';

@NgModule({
  declarations: [
    ConferenceApp,
    MapPage,
    ProgramacaoPage,
    ProgramacaoFilterPage,
    SessionDetailPage,
    SemanaAcadPage,
    TutorialPage,
    CredenciamentoPage,
    RoomPage,
    RoomDetailPage,
    ApOraisPage,
    ListMinicursosPage,
    MinicursoDetailPage,
    ListOficinaPage,
    OficinaDetailPage,
    MesaRedondaPage,
    LivroResumoPage,
    CadernoResumoPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(ConferenceApp, {
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
    }, {
        links: [
          { component: SemanaAcadPage, name: 'Semana', segment: 'semana' },
          { component: ProgramacaoPage, name: 'programacao', segment: 'programacao' },
          { component: SessionDetailPage, name: 'SessionDetail', segment: 'sessionDetail/:name' },
          { component: ProgramacaoFilterPage, name: 'programacaoFilter', segment: 'programacaoFilter' },
          { component: TutorialPage, name: 'tutorial', segment: 'tutorial' },
          { component: CredenciamentoPage, name: 'CredenciamentoPage', segment: 'credenciamento' },
          { component: MapPage, name: 'MapPage', segment: 'map' },
          { component: SemanaAcadPage, name: 'SemanaAcadPage', segment: 'semana-acad' },
          { component: RoomPage, name: 'RoomPage', segment: 'room-page' },
          { component: RoomDetailPage, name: 'RoomDetailPage', segment: 'room-detail-page' },
          { component: ApOraisPage, name: 'ApOraisPage', segment: 'ap-orais-page' },
          { component: ListMinicursosPage, name: 'ListMinicursosPage', segment: 'page-list-minicursos' },        
          { component: MinicursoDetailPage, name: 'MinicursoDetailPage', segment: 'page-minicurso-detail' },
          { component: ListOficinaPage, name: 'ListOficinaPage', segment: 'page-list-oficina' },
          { component: OficinaDetailPage, name: 'OficinaDetailPage', segment: 'page-oficina-detail' },
          { component: MesaRedondaPage, name: 'MesaRedondaPage', segment: 'page-mesa-redonda' },
          { component: LivroResumoPage, name: 'LivroResumoPage', segment: 'page-livro-resumo' },
          { component: CadernoResumoPage, name: 'CadernoResumoPage', segment: 'page-caderno-resumo' }
        ]
      }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    MapPage,
    ProgramacaoPage,
    ProgramacaoFilterPage,
    SessionDetailPage,
    SemanaAcadPage,
    TutorialPage,
    CredenciamentoPage,
    RoomPage,
    RoomDetailPage,
    ApOraisPage,
    ListMinicursosPage,
    MinicursoDetailPage,
    ListOficinaPage,
    OficinaDetailPage,
    MesaRedondaPage,
    LivroResumoPage,
    CadernoResumoPage
  ],  
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
    SplashScreen
  ]
})
export class AppModule { }
