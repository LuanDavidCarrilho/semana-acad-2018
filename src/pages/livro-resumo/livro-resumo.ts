import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-livro-resumo',
  templateUrl: 'livro-resumo.html',
})
export class LivroResumoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openSite(url: any) {
    window.open(url, '_system', 'location=no');
  }

}
