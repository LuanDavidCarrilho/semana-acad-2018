import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-semana-acad',
  templateUrl: 'semana-acad.html',
})
export class SemanaAcadPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App) {
  }
  openMap() {
    this.navCtrl.push(MapPage);
  } 
  
  openSite(url: any) {
    window.open(url, '_system', 'location=no');
  }
}
