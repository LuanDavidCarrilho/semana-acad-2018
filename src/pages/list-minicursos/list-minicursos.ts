import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { MinicursoDetailPage } from '../minicurso-detail/minicurso-detail';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-list-minicursos',
  templateUrl: 'list-minicursos.html',
})
export class ListMinicursosPage {
  sessions: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public confData: ConferenceData, ) {

    this.confData.getMiniCursos().subscribe((data: any) => {
      console.log(data);
      this.sessions = data;
    });
  }

  miniCursoSelected(session: any) {
    this.navCtrl.push(MinicursoDetailPage, session);
  }
  openMap() {
    this.navCtrl.push(MapPage);
  }
}
