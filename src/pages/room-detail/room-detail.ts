import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApOraisPage } from '../ap-orais/ap-orais';

@IonicPage()
@Component({
  selector: 'page-room-detail',
  templateUrl: 'room-detail.html',
})
export class RoomDetailPage {
  roomName: String;
  sessions  : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.roomName = navParams.data.name;
    this.sessions = navParams.data.apresentacoes;
  }

  apSelected(session: any) {
    this.navCtrl.push(ApOraisPage, session);
  }
}
