import { Component, ViewChild } from '@angular/core';
import { AlertController, App, List, ModalController, NavController, ToastController, LoadingController, Refresher } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { UserData } from '../../providers/user-data';
import { SessionDetailPage } from '../session-detail/session-detail';
import { ProgramacaoFilterPage } from '../programacao-filter/programacao-filter';
import { RoomPage } from '../room/room';
import { ListMinicursosPage } from '../list-minicursos/list-minicursos';
import { CredenciamentoPage } from '../credenciamento/credenciamento';
import { ListOficinaPage } from '../list-oficina/list-oficina';
import { MesaRedondaPage } from '../mesa-redonda/mesa-redonda';

@Component({
  selector: 'page-programacao',
  templateUrl: 'programacao.html'
})
export class ProgramacaoPage {
  // the list is a child of the programacao page
  // @ViewChild('programacaoList') gets a reference to the list
  // with the variable #programacaoList, `read: List` tells it to return
  // the List and not a reference to the element
  @ViewChild('programacaoList', { read: List }) programacaoList: List;

  dayIndex = 0;
  queryText = '';
  segment = 'all';
  excludeTracks: any = [];
  shownSessions: any = [];
  groups: any = [];
  confDate: string;

  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public confData: ConferenceData,
    public user: UserData,
  ) { }

  ionViewDidLoad() {
    this.app.setTitle('programacao');
    this.updateprogramacao();
  }

  updateprogramacao() {
    // Close any open sliding items when the programacao updates
    this.programacaoList && this.programacaoList.closeSlidingItems();

    this.confData.getTimeline(this.dayIndex, this.queryText, this.excludeTracks, this.segment).subscribe((data: any) => {
      this.shownSessions = data.shownSessions;
      this.groups = data.groups;
    });
  }

  presentFilter() {
    let modal = this.modalCtrl.create(ProgramacaoFilterPage, this.excludeTracks);
    modal.present();

    modal.onWillDismiss((data: any[]) => {
      if (data) {
        this.excludeTracks = data;
        this.updateprogramacao();
      }
    });

  }

  goToSessionDetail(sessionData: any) {
    // go to the session detail page
    // and pass in the session data
    if (sessionData.type === 'apresentacoesorais') {
      this.navCtrl.push(RoomPage, {
        name: sessionData.name,
        session: sessionData
      });
    } else if (sessionData.type === 'minicurso') {
      this.navCtrl.push(ListMinicursosPage, {
        name: sessionData.name,
        session: sessionData
      });
    } else if (sessionData.type === 'oficina') {
      this.navCtrl.push(ListOficinaPage, {
        name: sessionData.name,
        session: sessionData
      });
    } else if (sessionData.type === 'credenciamento') {
      this.navCtrl.push(CredenciamentoPage, {
        name: sessionData.name,
        session: sessionData
      });
    } else if (sessionData.type === 'mesaredonda') {
      this.navCtrl.push(MesaRedondaPage, {
        name: sessionData.name,
        session: sessionData
      });
    } else if (sessionData.type === 'circuito') {
      this.navCtrl.push(SessionDetailPage, {
        name: sessionData.name,
        session: sessionData
      });
    } else
      this.navCtrl.push(SessionDetailPage, {
        name: sessionData.name,
        session: sessionData
      });
  }

  doRefresh(refresher: Refresher) {
    this.confData.getTimeline(this.dayIndex, this.queryText, this.excludeTracks, this.segment).subscribe((data: any) => {
      this.shownSessions = data.shownSessions;
      this.groups = data.groups;

      // simulate a network request that would take longer
      // than just pulling from out local json file
      setTimeout(() => {
        refresher.complete();

        const toast = this.toastCtrl.create({
          message: 'As sessões foram atualizadas.',
          duration: 3000
        });
        toast.present();
      }, 1000);
    });
  }
}
