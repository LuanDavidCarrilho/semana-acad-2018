import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-caderno-resumo',
  templateUrl: 'caderno-resumo.html',
})
export class CadernoResumoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openSite(url: any) {
    window.open(url, '_system', 'location=no');
  }

}
