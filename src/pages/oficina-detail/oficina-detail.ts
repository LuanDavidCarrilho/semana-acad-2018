import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-oficina-detail',
  templateUrl: 'oficina-detail.html',
})
export class OficinaDetailPage {

  ofTitle: String;
  ofMinister: String;
  ofParticipant: String;
  ofLocation: String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.ofTitle = navParams.data.title;
    this.ofMinister = navParams.data.minister;
    this.ofParticipant = navParams.data.participant;
    this.ofLocation = navParams.data.location;
  }

}
