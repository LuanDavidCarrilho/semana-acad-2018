import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-minicurso-detail',
  templateUrl: 'minicurso-detail.html',
})
export class MinicursoDetailPage {

  mcTitle: String;
  mcSpeaker: String;
  mcDescription: String;
  mcParticipant: String;
  mcLocation: String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.mcTitle = navParams.data.title;
    this.mcSpeaker = navParams.data.speaker;
    this.mcDescription = navParams.data.description;
    this.mcParticipant = navParams.data.participant;
    this.mcLocation = navParams.data.location;
  }
}
