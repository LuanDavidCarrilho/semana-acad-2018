import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-credenciamento',
  templateUrl: 'credenciamento.html',
})
export class CredenciamentoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openMap() {
    this.navCtrl.push(MapPage);
  }
}
