import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-mesa-redonda',
  templateUrl: 'mesa-redonda.html',
})
export class MesaRedondaPage {

  mrTitle: String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.mrTitle = navParams.data.title;

  }

  openMap() {
    this.navCtrl.push(MapPage);
  }

}
