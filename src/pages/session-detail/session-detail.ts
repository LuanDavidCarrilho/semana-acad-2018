import { Component } from '@angular/core';

import { NavParams, NavController } from 'ionic-angular';
import { MapPage } from '../map/map';


@Component({
  selector: 'page-session-detail',
  templateUrl: 'session-detail.html'
})
export class SessionDetailPage {
  session: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.session = navParams.data.session;
  }

  openMap() {
    this.navCtrl.push(MapPage);
  }

  openPdf(url: any) {
    window.open(url, '_system', 'location=no');
  }
}
