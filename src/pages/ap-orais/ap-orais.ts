import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-ap-orais',
  templateUrl: 'ap-orais.html',
})
export class ApOraisPage {  
  apTitle: String;
  apOrientando: String;
  apOrientador: String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.apTitle = navParams.data.title;
    this.apOrientando = navParams.data.orientando;
    this.apOrientador = navParams.data.orientador;
  }
}