import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { OficinaDetailPage } from '../oficina-detail/oficina-detail';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-list-oficina',
  templateUrl: 'list-oficina.html',
})
export class ListOficinaPage {
  sessions: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public confData: ConferenceData, ) {

    this.confData.getOficinas().subscribe((data: any) => {
      console.log(data);
      this.sessions = data;
    });
  }

  oficinaSelected(session: any) {
    this.navCtrl.push(OficinaDetailPage, session);
  }
  openMap() {
    this.navCtrl.push(MapPage);
  }
}
